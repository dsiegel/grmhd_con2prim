#ifndef con2prim
#define con2prim

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <string.h>
#include <stdbool.h>
#include "con2prim_config.h"

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

#define RHO 0
#define v1_cov 1
#define v2_cov 2
#define v3_cov 3
#define EPS 4

#define D 0
#define S1_cov 1
#define S2_cov 2
#define S3_cov 3
#define TAU 4

#define B1_con 5
#define B2_con 6
#define B3_con 7

#define YE 8
#define TEMP 9
#define PRESS 10
#define WLORENTZ 11
#define ENT 12
#define A_BAR 13


#if STANDALONE
  // constants, variables and structures needed
  // by standalone version

  // some constants (required by EOS_Omni
  // in standalone version)
  extern const double kBerg;
  extern const double amu;

  // conversion factors between cgs and M_Sun = c = G = 1
  // (temperature between K and MeV)
  // see EOS_Omni/doc/units.py
  extern const double rho_gf;
  extern const double press_gf;
  extern const double eps_gf;
  extern const double temp_gf;
  
  // Inverses of the numbers above
  extern const double inv_rho_gf;
  extern const double inv_press_gf;
  extern const double inv_eps_gf;
  extern const double inv_temp_gf;


  // EOS related quantities

  // nuc eos

//  struct nuc_eos_vars {
//    int nrho;
//    int ntemp;
//    int nye;
//    double *fourtables;
//    double *logrho; 
//    double *logtemp;
//    double *yes;
//    double energy_shift;
//    double dtemp, dtempi;
//    double drho, drhoi;
//    double dye, dyei;
//    // min and max values
//    double eos_rhomax, eos_rhomin;
//    double eos_tempmin, eos_tempmax;
//    double eos_yemin, eos_yemax;
//    double eos_epsmin, eos_epsmax;
//    int *ivs_short;
//  };

#endif


struct metric {
    double lo[4][4];
    double up[4][4];
    double lo_det;
    double up_det;
    double lo_sqrt_det;
};


struct c2p_steer {
  int c2p_method;
  int c2p_method_backup;
  bool use_c2p_method_backup;
  int eoskey;
  int eoskey_polytrope;
  double eos_prec;
  double eos_rho_min;
  double eos_rho_max;
  double eos_temp_min;
  double eos_temp_max;
  double eos_eps_min;
  double eos_eps_max;
  double eos_press_min;
  double eos_press_max;
  int evolve_T;
  int evolve_Ye;
  double tol_x;
  double tol_x_retry;
  int max_iterations;
  int extra_iterations;
  double rho_atmo;
  double rho_atmo_tol;
  double T_atmo;
  double Ye_atmo;
  bool retain_B_atmo;
  int numprims;
  int numcons;
  bool enforce_v2;
};

struct c2p_steer c2p;

struct c2p_report {
  bool failed;
  bool adjust_cons;
  char err_msg[200];
  int count;
  bool retry;
  int c2p_keyerr;
  int nEOScalls;
};


// functions
                  
void con2prim_MHD_(double * prim, const double * con, const double g_up[4][4],
      const double g_lo[4][4], const bool key_excise, const bool c2p_grace,
      struct c2p_report * rep);                

void prim2con_MHD_(const double * prim, double * con, const double g_con[4][4],
  const double g_cov[4][4]);
		      
void NR_3D(struct c2p_report * c2p_rep, const double S_squared, const double BdotS, 
  const double B_squared, const double * con, double * prim, const double g_con[4][4], 
  const double g_cov[4][4], const double tol_x, int SAFEGUESS, int stepsize);
    
void palenzuela(struct c2p_report * c2p_rep, const double S_squared, const double BdotS, 
      const double B_squared, const double * con, double * prim, const double g_con[4][4], 
      const double g_cov[4][4], const double tol_x, bool use_epsmin);

void NR_2D_Noble(struct c2p_report * c2p_rep, const double S_squared, const double BdotS, 
      const double B_squared, const double * con, double * prim, const double g_con[4][4], 
      const double g_cov[4][4], const double tol_x);

double zbrent(double (*func)(double, double *, struct c2p_report *, bool, double *), double * param, double * temp_guess, double x1, double x2, double tol, struct c2p_report * c2p_rep, bool use_epsmin);

void newman(struct c2p_report * c2p_rep, const double S_squared, 
          const double BdotS, const double B_squared, const double * con, double * prim, 
	      const double g_cov[4][4], const double g_con[4][4], const double tol_x);

void NR_2D_rhoT(struct c2p_report * c2p_rep, const double S_squared, 
      const double BdotS, const double B_squared, const double * con, double * prim, 
    const double glo[4][4], const double tol_x, int SAFEGUESS, int stepsize);
    
void NR_2D_WT(struct c2p_report * c2p_rep, const double S_squared, 
      const double BdotS, const double B_squared, const double * con, double * prim, 
    const double glo[4][4], const double tol_x, int SAFEGUESS, int stepsize);

double P_from_prim(double * prim);

void EOSprim_from_rhoTYe(const int keytemp, double * prim);

void EOS_press(int eoskey, int keytemp, double rho, double * eps, 
        double * temp, double ye, double * prs, int * keyerr, int * nEOScalls);
        
void EOS_press_ent_abar(int eoskey, int keytemp, double rho, double * eps, 
        double * temp, double ye, double * prs, double * ent, double * abar, int * keyerr, int * nEOScalls);         

void EOS_eps_from_press(int eoskey, double rho, double * eps, 
        double * temp, double ye, double prs, int * keyerr);

void EOS_EP_dEdr_dEdt_dPdr_dPdt(double * x, const double * con, double * Eprim, 
    double * Pprim, double * dEdrho, double * dEdt, double * dPdrho, double * dPdt, int stepsize);

void EOS_EP_dEdr_dEdt_dPdr_dPdt_2D(const double rho2D, const double temp2D, const double * con, double * Eprim, 
    double * Pprim, double * dEdrho, double * dEdt, double * dPdrho, double * dPdt, int stepsize);

void EP_dEdW_dEdZ_dEdT(double * Eprim, double * Pprim, double * dEdW, double * dEdZ, double * dEdT, 
    double * dpdrho, double * dpdT, double * x, const double * con, int stepsize);
    
void EP_dPdW_dPdZ_dPdT(double * Eprim, double * Pprim, double * dPdW, double * dPdZ, double * dPdT, 
    double * dpdrho, double * dpdT, double * x, const double * con, int stepsize);    

void EOS_P_dPdrho_dPdeps(const double * x, const double * con, double * temp_guess, double * eps, double * press, double * dPdrho, 
          double * dPdeps, int * nEOS_calls);

void EOS_P_from_hrho_dPdrho_dPdeps(const double rho, const double enth, const double * con, 
          double * temp_guess, double * eps, double * press, double * dPdrho, 
          double * dPdeps, double * entr, double * abar, int * nEOS_calls);

void EOS_press_from_rhoenthalpy(int eoskey, int keytemp, double prec, double rho, double * eps, 
    double * temp, double ye, double * press, double * enth, int * anyerr, int * keyerr, int * nEOScalls);

#if STANDALONE

double T_from_prim(double * prim);
double eps_from_prim(double * prim);
    
void EOS_Omni_press(int eoskey, int keytemp, double prec, double rho, double * eps, 
    double * temp, double ye, double * press, int * keyerr, int * nEOScalls);
    
void EOS_Omni_press_ent_abar(int eoskey, int keytemp, double prec, double rho, double * eps, 
    double * temp, double ye, double * press, double * ent, double * abar, int * keyerr, int * nEOScalls);    
    
void EOS_Omni_EpsFromPress(int eoskey, int keytemp, double prec, double rho, double * eps, 
    double * temp, double ye, double press, int * keyerr);    
    
void EOS_Omni_dpdrho_dpdt_dedrho_dedt(int eoskey, int keytemp, double rho, double * temp, 
       double ye, double * eps, double * press, double * dpdrho, double * dpdt, double * dedrho, 
       double * dedt, int * keyerr, int stepsize);

void EOS_Omni_dpdrho_dpdeps(const int eos_key, const int keytemp, const double rho, double * temp, 
       const double ye, double * eps, double * press, double * dpdrho, double * dpdeps, int * keyerr,
       int stepsize, int * nEOScalls);

void EOS_Omni_dpdrho_dpdeps_hinv(const int eoskey, const double rho, const double enth, double * temp, 
       const double ye, double * eps, double * press, double * dpdrho, double * dpdeps, double * entr, 
       double * abar, int * keyerr, int stepsize, int * nEOScalls);

void EOS_Omni_dpdrho_dpdeps_dpdT_depsdT(const int eoskey, const int keytemp, const double rho, double * temp, 
       const double ye, double * eps, double * press, double * dpdrho, double * dpdeps, double * dpdt, double * depsdt, int * keyerr,
       int stepsize, int * nEOScalls);

void EOS_Omni_get_eos_limits(int eoskey, double * rho_min, double * rho_max, double * t_min, 
        double * t_max, double * ye_min, double * ye_max, double * eps_min, double * eps_max, double * press_min, double * press_max);

void EOS_Omni_press_from_rhoenthalpy(int eoskey, int keytemp, double prec, double rho, double * eps, 
    double * temp, double ye, double * press, double * ent, int * anyerr, int * keyerr, int * nEOScalls);


void nuc_eos_C_ReadTable(char* nuceos_table_name);

void nuc_eos_m_kt1_dpdrhoe_dpderho(const int *restrict n_in,
				     const double *restrict rho, 
				     double *restrict temp,
				     const double *restrict ye,
				     const double *restrict eps,
				     double *restrict dpdrhoe,
				     double *restrict dpderho,
				     const double *restrict prec,
				     int *restrict keyerr,
				     int *restrict anyerr);

void nuc_eos_m_kt1_dpdrhot_dedrhot(const int *restrict n_in, const double
	*restrict rho, double *restrict temp, const double *restrict
	ye, double *restrict eps, double *restrict eps2, double
	*restrict press, double *restrict dpdrho, double *restrict
	dpdt, double *restrict dedrho, double *restrict dedt, const
	double *restrict prec, int *restrict keyerr, int *restrict
	anyerr);

void nuc_eos_m_kt1_dpdrhot_dedrhot_dpdeps(const int *restrict n_in, const
		double *restrict rho, double *restrict temp, const double
		*restrict ye, double *restrict eps, double *restrict eps2,
		double *restrict press, double *restrict dpdrho, double
		*restrict dpdt, double *restrict dedrho, double *restrict dedt,
		double *restrict dpdeps, const double *restrict prec, int
		*restrict keyerr, int *restrict anyerr);
     
//void nuc_eos_P_only(double xrho, double *xtemp, double xye,
//		     double *xenr, double* xprs,
//		     int keytemp,
//		     int *keyerr,double rfeps,struct nuc_eos_vars *struct_ptr, int * nEOScalls);

void nuc_eos_m_kt1_press_eps(const int *restrict n_in,
		     const double *restrict rho, 
		     const double *restrict temp,
		     const double *restrict ye,
		     double *restrict eps,
		     double *restrict prs,
		     int *restrict keyerr,
		     int *restrict anyerr);

void nuc_eos_m_kt0_press(const int *restrict n_in,
			 const double *restrict rho, 
			 double *restrict temp,
			 const double *restrict ye,
			 const double *restrict eps,
			 double *restrict prs,
			 const double *restrict prec,
			 int *restrict keyerr,
			 int *restrict anyerr,
       int *restrict nEOScalls);
     
//void nuc_eos_C_findtemp(double lr, double lt0, double ye, 
//			double lepsin, double prec, double *ltout,
//			int *keyerrt,struct nuc_eos_vars *struct_ptr, int * nEOScalls);

//void nuc_eos_findtemp(const double lr, 
//		      const double lt0,
//		      const double ye,
//		      const double entin,
//		      const double prec,
//		      double *restrict ltout,
//		      int *keyerrt); 
		
//void nuc_eos_C_linterp_for_temp(double x, double y, double z,
//				double* f, double* ft, 
//				int nx, int ny, int nz, 
//				double* xt,double*yt, double* zt, 
//				double* dlepsdlt);

//void nuc_eos_C_linterp_some(double x, double y, double z,
//			    double* f, double* ft, 
//			    int* ivs,
//			    int nx, int ny, int nz, int nvars,
//			    double* xt,double*yt, double* zt,struct nuc_eos_vars *struct_ptr
// 			  );

void nuc_eos_C_linterp_one_d(const int* restrict idx, 
			   const double delx, 
			   const double dely,
			   const double delz,
			   const int iv,
			   double* restrict f,
                           double* restrict depsdrho, 
			   double* restrict depsdt, 
  			   double* restrict dPdrho, 
			   double* restrict dPdt);

void nuc_eos_C_linterp_one(const int* restrict idx, 
			   const double delx, 
			   const double dely,
			   const double delz,
			   double* restrict f,
			   const int iv);

void nuc_eos_C_linterp_one_linT_low(const int* restrict idx, 
				    const double delx, 
				    const double dely,
				    const double delz,
				    double* restrict f,
				    const int iv);

void nuc_eos_C_linterp_one_linT_low_eps(const int* restrict idx, 
					const double delx, 
					const double dely,
					const double delz,
					double* restrict f);


//void nuc_eos_C_linterp_some2(double x, double y, double z,
//			    double* f, double* ft, 
//			    int* ivs,
//			    int nx, int ny, int nz, int nvars,
//			    double* xt,double*yt, double* zt,struct nuc_eos_vars *struct_ptr,double* dlepsdlrho, double* dlepsdlt, double* dlPdlrho, double* dlPdlt
// 			  );
// 			  
//void nuc_eos_C_linterp_some3(double x, double y, double z,
//			    double* f, double* ft, 
//			    int* ivs,
//			    int nx, int ny, int nz, int nvars,
//			    double* xt,double*yt, double* zt,struct nuc_eos_vars *struct_ptr,double* dlepsdlrho, double* dlepsdlt, double* dlPdlrho, double* dlPdlt
// 			  );
//
//void nuc_eos_C_linterp_some4(double x, double y, double z,
//			    double* f, double* ft, 
//			    int* ivs,
//			    int nx, int ny, int nz, int nvars,
//			    double* xt,double*yt, double* zt,struct nuc_eos_vars *struct_ptr,double* dlepsdlrho, double* dlepsdlt, double* dlPdlrho, double* dlPdlt
// 			  );
#endif

#ifdef __cplusplus
}
#endif

#endif
