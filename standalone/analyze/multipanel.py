import sys
import os
import subprocess
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.cm as mplcm
import matplotlib.colors as mplcl
from numpy import *
import matplotlib.colors as mcolors

##############################################################################################
### User settings ###

# define keys to all possible EOS and recovery schemes
# this must be consistent with the output of con2prim_standalone.c
eos_name = {'2' : 'Ideal Gas', '4' : 'LS 220','8': 'Helmholtz'}
rs_name = {'1' : '3D NR', '3' : '2D NR rhoT', '4' : '2D NR', '5': '1D Brent', '6':'2D NR Noble', '7':'1D Newman'}

# setting general static order of EOS and recovery schemes for output (figure, print & file)
# should contain all recovery schemes and EOS from above
print_order_EOS = ['Ideal Gas', 'LS 220', 'Helmholtz']
print_order_RS = ['2D NR', '2D NR rhoT', '3D NR', '1D Brent', '2D NR Noble', '1D Newman']

# defining the different figures
choose_plot={0:"Number of iterations", 1:"Log(relative error)", 2: "Number of EOS Calls"}

# define associated colorbar ticks for these figures
# 0: #counts
# 1: relative error
# 2: #EOS calls
colbar_ticks = {0: [5,10,15,20,25,30], 1: [-14,-12,-10,-8,-6,-4], 2: [10,20,30,40,50,60,70,80] }

# define value ranges for colorbars
range_counts_plot = [1,30]
range_EOScalls_plot = [1,80]
range_logerror_plot = [-15, log10(5e-4)]

##############################################################################################


dic_stat_counts = {}
dic_stat_nEOScalls = {}
dic_stat_error = {}
dic_stat_recov = {}
dic_stat_conv = {}

for EOS in eos_name:
  dic_stat_counts[eos_name[EOS]] = {}
  dic_stat_nEOScalls[eos_name[EOS]] = {}
  dic_stat_error[eos_name[EOS]] = {}
  dic_stat_recov[eos_name[EOS]] = {}
  dic_stat_conv[eos_name[EOS]] = {}
#


def std_page_setup():
  fig_width = 8.0
  fig_height = fig_width * .62
  params = {
    'figure.figsize' : [fig_width,fig_height],
    'figure.subplot.left' : 0.1,
    'figure.subplot.right' : .99,
    'figure.subplot.top' : 0.955,
    'figure.subplot.bottom' : 0.095,
    'figure.subplot.hspace' : 0.22,
    'figure.subplot.wspace' : 0.03,
    'lines.markersize': 6,
    'axes.labelsize': 14,
    'text.usetex':True,
    'font.size': 11,
    'legend.fontsize': 10,
    'xtick.labelsize': 13,
    'ytick.labelsize': 13,
    'axes.formatter.limits': [-2,3],
    'savefig.dpi': 320,
    'lines.antialiased': True
  }
  matplotlib.rcParams.update(params)
#

def multi_savefig(name, formats, fig=None):
  for fmt in formats.split(','):
    fname = "%s.%s" % (name, fmt)
    if (fig == None):
      plt.savefig(fname,dpi=300)
    else:
      fig.savefig(fname,dpi=300)

def find_max_counts(data):
  count_1D = data[:,4]
  recov_1D = data[:,2]

  return max(count_1D*recov_1D)

def find_max_neoscalls(data):
  neoscalls_1D = data[:,11]
  recov_1D = data[:,2]  
  return max(neoscalls_1D*recov_1D)

def find_min_error(data):
  error_1D = data[:,10]
  vmax2D = abs(error_1D)
  vmax2D = vmax2D[isfinite(vmax2D)]
  vmax2D = vmax2D[vmax2D > 0.]
  if len(vmax2D)==0:
    vmax2D=array([0])
    ans = vmax2D
  else:
    error_1D[isnan(error_1D)] = vmax2D.max()
    error_1D[isinf(error_1D)] = vmax2D.max()
    ans = vmax2D.min()

  return ans

def find_max_error(data):
  error_1D = data[:,10]
  vmax2D = abs(error_1D)
  vmax2D = vmax2D[isfinite(vmax2D)]
  vmax2D = vmax2D[vmax2D > 0.]

  error_1D[isnan(error_1D)] = vmax2D.max()
  error_1D[isinf(error_1D)] = vmax2D.max()
  return vmax2D.max()

def plot_from(fname_output_file, axis, ax_choice, plotted, minmax):
  EOS     = fname_output_file.split('_')[4].split('-')[1]
  aligned = fname_output_file.split('_')[-2]
  RS      = fname_output_file.split('_')[3].split('-')[1]


  # print some info
  print '*************************'
  print 'EOS (id): %s (%s)' % (eos_name[EOS], EOS)
  print 'recovery scheme (id): %s (%s)' % (rs_name[RS], RS)

  # load data
  data = loadtxt(fname_output_file)
  q1_1D = data[:,0] 
  q2_1D = data[:,1]
  recov_1D = data[:,2]
  conv_1D = data[:,3] 
  count_1D = data[:,4]
  epsneg_1D = data[:,5]
  gtc_1D = data[:,6]
  keyerr_1D = data[:,7]
  rho_1D = data[:,8]
  temp_1D = data[:,9]
  error_1D = data[:,10]
  neoscalls_1D = data[:,11]

  MeVinK = 8.6217e-11

  temp_1D= temp_1D-log10(MeVinK)

  # transform into 2D arrays
  # with q1 being x axis
  # and  q2 being y axis
  n_dat = len(q1_1D)
  n2D = int(sqrt(n_dat))
  q1 = q1_1D.reshape(n2D,n2D)
  q2 = q2_1D.reshape(n2D,n2D)
  recov = recov_1D.reshape(n2D,n2D)
  conv = conv_1D.reshape(n2D,n2D)
  count = count_1D.reshape(n2D,n2D)
  epsneg = epsneg_1D.reshape(n2D,n2D)
  gtc = gtc_1D.reshape(n2D,n2D)
  keyerr = keyerr_1D.reshape(n2D,n2D)
  rho = rho_1D.reshape(n2D,n2D)
  temp = temp_1D.reshape(n2D,n2D)
  error = error_1D.reshape(n2D,n2D)
  neoscalls = neoscalls_1D.reshape(n2D,n2D)
  q1min = q1.min()
  q1max = q1.max()
  q2min = q2.min()
  q2max = q2.max()
  p1min = rho.min()
  p1max = rho.max()
  p2min = temp.min()
  p2max = temp.max()

  print 'mean values for recovered points:'
  stat_counts = count*recov
  stat_nEOScalls = neoscalls*recov
  stat_error = error*recov
  
  print 'counts:', mean(stat_counts[stat_counts>0])
  print 'nEOS calls:',  mean(stat_nEOScalls[stat_nEOScalls>0])
  print 'error: %1.3e' % 10**(mean(log10(stat_error[stat_error>0])))
  print '*************************'

  dic_stat_counts[eos_name[EOS]][rs_name[RS]] = mean(stat_counts[stat_counts>0])
  dic_stat_nEOScalls[eos_name[EOS]][rs_name[RS]] = mean(stat_nEOScalls[stat_nEOScalls>0])
  dic_stat_error[eos_name[EOS]][rs_name[RS]] = 10**(mean(log10(stat_error[stat_error>0])))
  dic_stat_recov[eos_name[EOS]][rs_name[RS]] = sum(recov)/sum(ones_like(recov))*100.
  dic_stat_conv[eos_name[EOS]][rs_name[RS]] = sum(1-conv)/sum(ones_like(1-conv))*100.

  if ax_choice==0:
    ext = [q1min, q1max, q2min, q2max]
  else:
    ext = [p1min, p1max, p2min, p2max]

  aspect = 'auto'
  colmap = 'rainbow'
  cmap = plt.get_cmap(colmap)
  cmap.set_under('white')
  cmap.set_over('k')

  if plotted==0:
    colors1 = plt.cm.gist_rainbow_r(linspace(  .05, .2,  600))
    colors2 = plt.cm.gist_rainbow_r(linspace(.2,  .75,  500))
    colors3 = plt.cm.gist_rainbow_r(linspace(.75,   1, 1000))
    colors = vstack((colors1, colors2, colors3))
    
    cmap = mcolors.LinearSegmentedColormap.from_list('colormap', colors)
    cmap.set_under('white')
    cmap.set_over('k')
    plotted_array=(count[:,::-1].transpose()*recov[:,::-1].transpose(),range_counts_plot[0], range_counts_plot[1])

  elif plotted==1:

    vmax2D = abs(error)
    vmax2D = vmax2D[isfinite(vmax2D)]
    vmax2D = vmax2D[vmax2D > 0.]
    if len(vmax2D)==0:
      vmax2D=array([0]*len(vmax2D))
    else:
      error[isnan(error)] = vmax2D.max()
      error[isinf(error)] = vmax2D.max()

    colors1 = plt.cm.viridis(linspace(  .1, 1,  800))
    colors2 = plt.cm.inferno_r(linspace(.067,  .9,  800))
    colors = vstack((colors1, colors2))

    cmap = mcolors.LinearSegmentedColormap.from_list('colormap', colors)

    cmap.set_over('white')
    
    plotted_array = (log10( error[:,::-1].transpose() ), range_logerror_plot[0], range_logerror_plot[1])
    try:
      con3 = axis.contour(rho[:,::-1].transpose(), temp[:,::-1].transpose(), recov[:,::-1].transpose(), 1, colors='k')
      SHAPE={}
      for level in con3.collections:
        for kp,path in reversed(list(enumerate(level.get_paths()))):
          # go in reversed order due to deletions!

          # include test for "smallness" of your choice here:
          # using a simple estimation for the diameter based on the
          # x and y diameter...
          verts = path.vertices # (N,2)-shape array of contour line coordinates
          diameter = max(verts.max(axis=0) - verts.min(axis=0))
          SHAPE[kp] = shape(verts)[0]
          if diameter<2: # threshold to be refined for your actual dimensions!
            del(level.get_paths()[kp])  # no remove() for Path objects:(
    except ValueError:  #raised if `con3` is empty.
      pass
    
  elif plotted == 2:

    colmap = 'rainbow'
    colors1 = plt.cm.rainbow(linspace(0, .75, 200))
    colors2 = plt.cm.rainbow(linspace(.75, 1, 500))
    colors = vstack((colors1, colors2))
    
    cmap = mcolors.LinearSegmentedColormap.from_list('colormap', colors)

    cmap.set_under('white')
    cmap.set_over('k')
    plotted_array=(neoscalls[:,::-1].transpose()*recov[:,::-1].transpose(), range_EOScalls_plot[0], range_EOScalls_plot[1])
  #

  image= axis.imshow(plotted_array[0], extent=ext, interpolation='bilinear', cmap=cmap,
          vmin=plotted_array[1], vmax=plotted_array[2], aspect=aspect)
  axis.tick_params(direction='in')

  return image
#



####################################
###           Plotting           ###
####################################


MAGNITUDE =[[None]]*3
MAGNITUDE[0].pop(0)
minmax= [None]*3
EOSlist=[]
RSlist=[]
plots={}
filelist=[]
filelist_ = [x[2] for x in os.walk(os.getcwd())][0]

#extract info from filename
for f in filelist_:
  if f[-4:]=='.dat':
    filelist.append(f)
    eos = f.split('_')[4].split('-')[1]
    rs  = f.split('_')[3].split('-')[1]
    if eos not in EOSlist:
      EOSlist.append(eos)
    if rs not in RSlist:
      RSlist.append(rs)
    plots[(eos,rs)] = f
    data = loadtxt(f)
    print f
    #different minmax's for each choose_plot value
    MAGNITUDE[0].append(find_max_counts(data))
    minmax[0]=(1,max(MAGNITUDE[0]))
    MAGNITUDE[1].append(find_min_error(data))
    minmax[1]=(min(MAGNITUDE[1]),1e-1)
    MAGNITUDE[2].append(find_max_neoscalls(data))
    minmax[2]=(1,max(MAGNITUDE[2]))
  #
#

### setting the order of subplots ###

# sort recovery schemes accoring to order in print_order_RS
EOSlist_new = []
for nEOS, EOSname in enumerate(print_order_EOS):
  EOSID = eos_name.keys()[eos_name.values().index(EOSname)]
  if EOSID in EOSlist:
    EOSlist_new.append(EOSID)
  #
#
EOSlist = EOSlist_new

# sort recovery schemes accoring to order in print_order_RS
RSlist_new = []
for nRS, RSname in enumerate(print_order_RS):
  RSID = rs_name.keys()[rs_name.values().index(RSname)]
  if RSID in RSlist:
    RSlist_new.append(RSID)
  #
#
RSlist = RSlist_new

print "minmax: ", minmax[1]
print "minmax: ", min(MAGNITUDE[1])

#rho vs. T plot?
if filelist[0].split('_')[5].split('-')[0]== 'RHO':
  rhoT = 0
  print "(Plotting logW-1 vs. Pmag/P: (rhoT= ", rhoT," )"
else:
  rhoT = 1
  print "(Plotting rho vs. T: (rhoT= ", rhoT," )"
#

#titling plot, naming pdf
par1 = filelist[0][:-4].split('_')[5].split('-')
par2 = filelist[0][:-4].split('_')[6].split('-')
par3 = filelist[0][:-4].split('_')[7].split('-')

param1 = par1[0] +  "-"  +"-".join(par1[1:]) 
param2 = par2[0]+  "-"  + "-".join(par2[1:])
param3 = par3[0]+  "-"  + "-".join(par3[1:])

pdfname = param1+"_"+param2
params = par1[0]+"= " +"-".join(par1[1:]) +", "+par2[0]+"= "+ "-".join(par2[1:])

if par3[1]!='0.1':
  pdfname+= "_"+param3
#

std_page_setup()

if len(EOSlist)==2:
  fig_width = 8.0
  fig_height = fig_width * .45 #1.618
  params={'figure.subplot.top' : 0.93, 'figure.subplot.bottom' : 0.14,
    'figure.figsize' : [fig_width,fig_height]}
  matplotlib.rcParams.update(params)
elif len(EOSlist)==1:
  fig_width = 8.0
  fig_height = fig_width * .25 #1.618
  params={'figure.subplot.top' : 0.84, 'figure.subplot.bottom' : 0.21,
    'figure.figsize' : [fig_width,fig_height]}
  matplotlib.rcParams.update(params)
# 

figlist=[None]*3
axlist = [None]*3
imlist= [None]*3
cbar_ax = [None]*3
for x in range(len(figlist)):
  FIG, (AX) = plt.subplots(len(EOSlist),len(RSlist), sharey='row')
  figlist[x]=FIG
  axlist[x]= AX
#  

if rhoT!=0:
  y_label = ('$ \\log (T [\\mathrm{K}])$')
  x_label = ('$ \\log (\\rho [\\mathrm{g}/\\mathrm{cm}^{3}])$')
else:
  y_label = ('$ \\log (p_\\mathrm{mag}/p)$')
  x_label = ('$ \\log (W-1)$')
#

for CP in range(3):
  for p in plots:
    x= EOSlist.index(p[0])
    y=RSlist.index(p[1])
    AX=axlist[CP]
    print "x=", x

    if len(EOSlist)>1 and len(RSlist)==1:
      ax=AX[x]
    elif len(EOSlist)==1 and len(RSlist)>1:
      ax=AX[y]
    elif len(EOSlist)>1 and len(RSlist)>1:
      ax=AX[x,y]
    else:
      ax=AX

    imlist[CP] = plot_from(plots[p], ax, rhoT, CP, minmax[CP])

    if rhoT:
      dic_yticks = {0: [6,8,10], 1: [9,10,11], 2: [6,8,10]}
      dic_xticks = {0: [5,10,15], 1: [5,10,15], 2: [5,10]}
      ax.set_xticks(dic_xticks[x])
      ax.set_xticklabels(dic_xticks[x])
    else:
      dic_xticks = {0: [-2,0,2], 1: [-2,0,2], 2: [-2,0,2]}
      dic_yticks = {0: [-5,0,5,10], 1: [-5,0,5,10], 2: [-5,0,5,10]}
    if y==0:
      ax.set_ylabel(eos_name[p[0]]+"\n"+y_label)
      #ax.yaxis.set_label_coords(-0.25, .5)
      ax.set_yticks(dic_yticks[x])
      ax.set_yticklabels(dic_yticks[x])
    #  

    if x==len(EOSlist)-1:
      ax.xaxis.set_label_coords(0.5, -0.2)
      ax.set_xlabel(x_label)
    #  
    if x==0:
      ax.set_title(rs_name[p[1]] )
    #
    ax.set_xticks(dic_xticks[x])
    ax.set_xticklabels(dic_xticks[x])
  #  
#

name_extra=["_n_iter", "_rel_err", "_neos"]

for CP in range(3):
  FIG= figlist[CP]
  im = imlist[CP]
  FIG.subplots_adjust(right=0.87)
  cbar_ax[CP] = FIG.add_axes([0.89, 0.11, 0.02, 0.77])
  cbar = FIG.colorbar(im, cax=cbar_ax[CP], extend= 'max', label=choose_plot[CP], ticks=colbar_ticks[CP])
  cbar.ax.set_yticklabels(colbar_ticks[CP])
  multi_savefig(pdfname+name_extra[CP], 'pdf', fig=FIG)
#

binedges=linspace(0,400,num=21)

for f in filelist_:
  if f[-4:]=='.dat':
    eos = f.split('_')[4].split('-')[1]
    rs  = f.split('_')[3].split('-')[1]
    if eos=='8' and rs=='1':
      data = loadtxt(f)
      neoscalls_1D = data[:,11]
      recov_1D = data[:,2]
      counts, bins, ignored=plt.hist(neoscalls_1D*recov_1D, bins=binedges)
    #
  #
#

####################################
###   Print & store statistics   ###
####################################

stats_counts_arr = zeros((len(print_order_EOS), len(print_order_RS)))
stats_nEOScalls_arr = zeros((len(print_order_EOS), len(print_order_RS)))
stats_error_arr = zeros((len(print_order_EOS), len(print_order_RS)))
stats_recov_arr = zeros((len(print_order_EOS), len(print_order_RS)))
stats_conv_arr = zeros((len(print_order_EOS), len(print_order_RS)))

for nEOS, EOS in enumerate(print_order_EOS):
  for nRS, RS in enumerate(print_order_RS):
    if (RS in dic_stat_counts.values()[0]):
      stats_counts_arr[nEOS,nRS] = dic_stat_counts[EOS][RS]
      stats_nEOScalls_arr[nEOS,nRS] = dic_stat_nEOScalls[EOS][RS]
      stats_error_arr[nEOS,nRS] = dic_stat_error[EOS][RS]
      stats_recov_arr[nEOS,nRS] = dic_stat_recov[EOS][RS]
      stats_conv_arr[nEOS,nRS] = dic_stat_conv[EOS][RS]
    #  
  #
#

# print statistics on screen
print '---------------------------------------------------------------------'
print 'Statistics summary:\n'
print 'Counts:'
label_schemes = '         '
for RS in print_order_RS:
  label_schemes += (RS + '  ')
#
set_printoptions(precision=2)
print label_schemes
for nEOS, EOS in enumerate(print_order_EOS):
  print EOS, stats_counts_arr[nEOS,:]
#

print '\n#EOS calls:'
print label_schemes
for nEOS, EOS in enumerate(print_order_EOS):
  print EOS, stats_nEOScalls_arr[nEOS,:]
#

print '\nError:'
print label_schemes
for nEOS, EOS in enumerate(print_order_EOS):
  print EOS, stats_error_arr[nEOS,:]
#

print '\n % recovered:'
print label_schemes
for nEOS, EOS in enumerate(print_order_EOS):
  print EOS, stats_recov_arr[nEOS,:]
#

print '\n % converged:'
print label_schemes
for nEOS, EOS in enumerate(print_order_EOS):
  print EOS, stats_conv_arr[nEOS,:]
#
print '---------------------------------------------------------------------'


# output ascii file with statistics
set_printoptions(precision=1)
f1 = open('statistics_summary.txt', 'w')
f1.write('Recovery test statistics summary: \n\n')

f1.write(label_schemes + '\n\n')

f1.write('Counts:\n')
for nEOS, EOS in enumerate(print_order_EOS):
  f1.write(EOS + '   '+ str(stats_counts_arr[nEOS,:]) + '\n')
#

f1.write('\n#EOS calls:\n')
for nEOS, EOS in enumerate(print_order_EOS):
  f1.write(EOS + '   '+ str(stats_nEOScalls_arr[nEOS,:]) + '\n')
#

f1.write('\nAccuracy:\n')
for nEOS, EOS in enumerate(print_order_EOS):
  f1.write(EOS + '   '+ str(stats_error_arr[nEOS,:]) + '\n')
#

f1.write('\n% recoverd:\n')
for nEOS, EOS in enumerate(print_order_EOS):
  f1.write(EOS + '   '+ str(stats_recov_arr[nEOS,:]) + '\n')
#

f1.write('\n% converged:\n')
for nEOS, EOS in enumerate(print_order_EOS):
  f1.write(EOS + '   '+ str(stats_conv_arr[nEOS,:]) + '\n')
#
f1.close()

print 'Done.'
