import sys
import os
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import matplotlib.cm as mplcm
import matplotlib.colors as mplcl
from numpy import *

# USAGE: python analyze_all.py A1 A2 A3 A4

# A1: log10(W-1) or log10(rho)
# A2: log10(p_mag/p_gas) or log10(temp)
# A3: Ye
# A4: rhoTkey (1 or 0)

# set path to standalone executable here
dir_with_standalone = '.'

# set lists with recovery schemes and EOS to consider here
recovery_schemes = [1,4,5,6,7]
equations_of_state = [2,4,8]

# parameters
rhoTkey= int(sys.argv[4])
if rhoTkey==1:
  W = double(sys.argv[1])
  P = double(sys.argv[2])
else:
  RHO = double(sys.argv[1])
  T   = double(sys.argv[2])
YE = double(sys.argv[3])

os.chdir(dir_with_standalone)
executable = './con2prim_standalone'

for RS in recovery_schemes:
  for EOS in equations_of_state:
    if rhoTkey==1:
      os.system("%s %i %i %i %i %.1f %i" % (executable, RS, EOS, W, P, YE, rhoTkey))
    else:
      os.system("%s %i %i %.1e %.1e %.1f %i" %(executable, RS, EOS, RHO, T, YE, rhoTkey))
    #
  #
#  
