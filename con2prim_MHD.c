/*@@
   @file      con2prim_MHD.c
   @date      Sep 17, 2016
   @author    Daniel Siegel, Philipp Moesta
   @desc 
   This file provides the main con2prim routine con2prim_MHD_ along with some utilities,
   which handle the actual recovery process and error policy. Note that this code is 
   independent of the CACTUS infrastructure and does neither assume a specific con2prim 
   scheme nor a specific EOS. Presently, the code calls a recovery routine based on 
   3D Newton-Raphson root finding, which implements a scheme based on 
   Cerda-Duran et al. (2008) that should work with any EOS given in terms of rho, T, Ye.
   However, any other recovery scheme can be used, in principle. Note that this code 
   can be directly tested with the attached standalone wrapper by setting STANDALONE 1 
   in the header file con2prim.h.
   @enddesc 
 @@*/

#include "con2prim.h"

#if !STANDALONE
  #include "cctk_Arguments.h"
  #include "cctk_Parameters.h"
  #include "cctk_Functions.h"
#endif

extern struct c2p_steer c2p;


void calc_B2_S2_BdotS(const double * con, const double g_con[4][4], 
      const double g_cov[4][4], double * B_squared, double * S_squared, double * BdotS)
{
   
  double B1_cov, B2_cov, B3_cov, S1_con, S2_con, S3_con;  
      
  // Lower indices - covariant
  B1_cov = g_cov[1][1]*con[B1_con]+g_cov[1][2]*con[B2_con]+g_cov[1][3]*con[B3_con];
  B2_cov = g_cov[1][2]*con[B1_con]+g_cov[2][2]*con[B2_con]+g_cov[2][3]*con[B3_con];
  B3_cov = g_cov[1][3]*con[B1_con]+g_cov[2][3]*con[B2_con]+g_cov[3][3]*con[B3_con];
  
  // Raise indices - contravariant
  S1_con = g_con[1][1]*con[S1_cov]+g_con[1][2]*con[S2_cov]+g_con[1][3]*con[S3_cov];
  S2_con = g_con[1][2]*con[S1_cov]+g_con[2][2]*con[S2_cov]+g_con[2][3]*con[S3_cov];
  S3_con = g_con[1][3]*con[S1_cov]+g_con[2][3]*con[S2_cov]+g_con[3][3]*con[S3_cov];
 
  // Need to calculate for (21) and (22) in Cerda-Duran 2008
  // B * S = B^i * S_i
  *BdotS = con[B1_con]*con[S1_cov] + con[B2_con]*con[S2_cov] + con[B3_con]*con[S3_cov];
  
  // B^2 = B^i * B_i
  *B_squared = con[B1_con]*B1_cov + con[B2_con]*B2_cov + con[B3_con]*B3_cov;
  
  // S^2 = S^i * S_i
  *S_squared = S1_con*con[S1_cov] + S2_con*con[S2_cov] + S3_con*con[S3_cov];
 
}


void reset_to_atmosphere(double * prim, const double * con)
{
  
  // reset primitives to atmosphere values
  
  prim[RHO] = c2p.rho_atmo;
  prim[v1_cov] = 0.0;
  prim[v2_cov] = 0.0;
  prim[v3_cov] = 0.0;
  prim[WLORENTZ] = 1.0;
  
  if ((c2p.retain_B_atmo) && !(con[B1_con] != con[B1_con]) && !(con[B2_con] != con[B2_con])
        && !(con[B3_con] != con[B3_con])) {
    prim[B1_con] = con[B1_con];
    prim[B2_con] = con[B2_con];
    prim[B3_con] = con[B3_con];
  } else {
    prim[B1_con] = 0.0;
    prim[B2_con] = 0.0;
    prim[B3_con] = 0.0;
  }

  int keyerr = 0;
  int nEOScalls;
  double xrho = prim[RHO];
  double xeps = 0.0;
  double xpress = 0.0;
  int keytemp;
  double xtemp,xye;
  
  if (c2p.evolve_T){
    prim[TEMP] = c2p.T_atmo;
    prim[YE] = c2p.Ye_atmo;
    
    // compute eps, press
    keytemp = 1;
    xtemp = prim[TEMP];
    xye = prim[YE];
    double xent = 0.0;
    double xabar = 0.0;
    
    EOS_press_ent_abar(c2p.eoskey,keytemp,xrho,&xeps,&xtemp,xye,
            &xpress,&xent,&xabar,&keyerr,&nEOScalls);

    prim[ENT] = xent;
    prim[A_BAR] = xabar;

  } else {
    // compute eps, press
    keytemp = 0;
    xtemp = 0.0;
    xye = 0.0;
    
    EOS_press(c2p.eoskey_polytrope,keytemp,
                   xrho,&xeps,&xtemp,xye,&xpress,&keyerr,&nEOScalls);
    EOS_eps_from_press(c2p.eoskey_polytrope,
                   xrho,&xeps,&xtemp,xye,xpress,&keyerr);
  }
  
  prim[EPS] = xeps;
  prim[PRESS] = xpress;
  
}


void reset_velocity(const double gup[4][4], double * prim)
{

  /* Adjust initial guess for 3-velocity to ensure v^2 <= 1. Since the
   * metric is different from the previous time step, but the velocity components
   * as initial guesses are taken from the previous time step, v^2 <= 1 is not
   * guaranteed anymore with the present metric components.
   */

  double velupx = gup[1][1]*prim[v1_cov] + gup[1][2]*prim[v2_cov] + gup[1][3]*prim[v3_cov];
  double velupy = gup[2][1]*prim[v1_cov] + gup[2][2]*prim[v2_cov] + gup[2][3]*prim[v3_cov];
  double velupz = gup[3][1]*prim[v1_cov] + gup[3][2]*prim[v2_cov] + gup[3][3]*prim[v3_cov];

  double v2 = velupx*prim[v1_cov] + velupy*prim[v2_cov] + velupz*prim[v3_cov];
  
  if (v2 >= 1.0){
    prim[v1_cov] *= 1.0 / (v2*(1.0e0+1.0e-10));
    prim[v2_cov] *= 1.0 / (v2*(1.0e0+1.0e-10));
    prim[v3_cov] *= 1.0 / (v2*(1.0e0+1.0e-10));

    velupx = gup[1][1]*prim[v1_cov] + gup[1][2]*prim[v2_cov] + gup[1][3]*prim[v3_cov];
    velupy = gup[2][1]*prim[v1_cov] + gup[2][2]*prim[v2_cov] + gup[2][3]*prim[v3_cov];
    velupz = gup[3][1]*prim[v1_cov] + gup[3][2]*prim[v2_cov] + gup[3][3]*prim[v3_cov];

    v2 = velupx*prim[v1_cov] + velupy*prim[v2_cov] + velupz*prim[v3_cov];
    prim[WLORENTZ] = 1.0 / sqrt(1.0 - v2);
  } else if (v2 < 0.0){
    prim[v1_cov] = 0.0;
    prim[v2_cov] = 0.0;
    prim[v3_cov] = 0.0;
    prim[WLORENTZ] = 1.0;

  }
}

void reset_temperature(double * prim)
{
  // reset temperature to atmosphere level and update
  // affected primitives for consistency
  
  int keyerr = 0;
  int keytemp = 1;
  int nEOScalls;
  double xrho = prim[RHO];
  double xeps = 0.0;
  double xpress = 0.0;
  double xtemp = c2p.T_atmo;
  double xye = prim[YE];
  

  if (c2p.evolve_T){
    double xent = 0.0;
    double xabar = 0.0;
    EOS_press_ent_abar(c2p.eoskey,keytemp,xrho,&xeps,&xtemp,xye,&xpress,&xent,&xabar,&keyerr,&nEOScalls);
    prim[ENT] = xent;
    prim[A_BAR] = xabar;
  } else {
    EOS_press(c2p.eoskey,keytemp,xrho,&xeps,&xtemp,xye,&xpress,&keyerr,&nEOScalls);
  }

  // update primitives
  prim[EPS]   = xeps;
  prim[TEMP]  = xtemp;
  prim[PRESS] = xpress;
}


void c2p_recovery_scheme(const int c2p_method, struct c2p_report * c2p_rep, 
      const double S_squared, const double BdotS, const double B_squared, 
      const double * con, double * prim, const double original_prim[c2p.numprims], 
      const double g_up[4][4], const double g_lo[4][4], const double tol_x )
{

  // This routine selects the actual con2prim recovery scheme, any scheme can be called
  
  /*
  Note concerning Newton-Raphson root-finding schemes:
    Handling of initial guesses for primitives in con2prim
    SAFEGUESS = 0: use primitives from previous time step (default)
    SAFEGUESS = 1: use "safe guess" values from Cerdá-Durán 2008, Eq.(39)-(42)
    only used if SAFEGUESS=0 did not converge (see below)
    
  Note: stepsize parameter only needed for nuc EOS routines and Newton-Raphson root
    finding (although just a dummy parameter right now)  
  
  */

  switch(c2p_method) {
      case 1 :
        // 3D Newton-Raphson based on Cerdá-Durán 2008, Eq.(21),(22),(29) [method=1], i.e.
        // adding eps-eps(rho,T) = 0 as a third equation, or
        {
          int stepsize=1;
          int SAFEGUESS=0;
          NR_3D(c2p_rep,S_squared,BdotS,B_squared,con,prim,g_up,g_lo,
            tol_x,SAFEGUESS,stepsize);

          // restrict eps
          //if( (c2p_rep->failed) && (c2p.eoskey != 1) ){
          //  use_epsmin = true;
          //  // reset primitives (initial guesses)
          //  for(int l=0;l<c2p.numprims;l++) {
          //    prim[l] = original_prim[l];
          //  }
          //  NR_3D(c2p_rep,S_squared,BdotS,B_squared,con,prim,g_up,g_lo,
          //      tol_x,use_epsmin,SAFEGUESS,stepsize);
          //  use_epsmin = false;
          //}

          // retry with safe guess initial values
          // if(c2p_rep->failed){
          //   SAFEGUESS=1;
          //   use_epsmin = (c2p.eoskey == 1);
          //   NR_3D(c2p_rep,S_squared,BdotS,B_squared,con,prim,g_up,g_lo,
          //           tol_x,use_epsmin,SAFEGUESS,stepsize);
          //   if ((c2p_rep->failed) && (c2p.c2p_method == 1) && (c2p.eoskey != 1)){
          //     // try with restricted eps
          //    use_epsmin = true;
          //    NR_3D(c2p_rep,S_squared,BdotS,B_squared,con,prim,g_up,g_lo,
          //       tol_x,use_epsmin,SAFEGUESS,stepsize);
          //   }        
          // }
          break;
        }
      case 2 :
        // 3D Newton-Raphson based on Cerdá-Durán 2008, Eq.(21),(22),(26) [method=2], i.e.
        // adding P-P(rho,T) = 0 as a third equation
        {
          int stepsize=1;
          int SAFEGUESS=0;
          NR_3D(c2p_rep,S_squared,BdotS,B_squared,con,prim,g_up,g_lo,
            tol_x,SAFEGUESS,stepsize);
        
          // retry with safe guess initial values
          if(c2p_rep->failed){
            SAFEGUESS=1;
            NR_3D(c2p_rep,S_squared,BdotS,B_squared,con,prim,g_up,g_lo,
              tol_x,SAFEGUESS,stepsize);
          }
          break;
        }
      case 3 :
        // variant of 2D Newton-Raphson based on Cerdá-Durán 2008, Eq.(21),(22) using
        // rest-mass density and temperature as root finding variables
        {
          int stepsize=1;
          int SAFEGUESS=0;
          NR_2D_rhoT(c2p_rep,S_squared,BdotS,B_squared,con,prim,g_lo,
              tol_x,SAFEGUESS,stepsize);
    
          // retry with safe guess initial values
          if(c2p_rep->failed){
            SAFEGUESS=1;
            NR_2D_rhoT(c2p_rep,S_squared,BdotS,B_squared,con,prim,g_lo,
              tol_x,SAFEGUESS,stepsize);
          }
          break;
        }
      case 4 :
        // variant of 2D Newton-Raphson based on Cerdá-Durán 2008, Eq.(21),(22) using
        // Lorentz factor and temperature as root finding variables
        {
          int stepsize=1;
          int SAFEGUESS=0;
          NR_2D_WT(c2p_rep,S_squared,BdotS,B_squared,con,prim,g_lo,
              tol_x,SAFEGUESS,stepsize);
    
          // retry with safe guess initial values
          if(c2p_rep->failed){
            SAFEGUESS=1;
            NR_2D_WT(c2p_rep,S_squared,BdotS,B_squared,con,prim,g_lo,
              tol_x,SAFEGUESS,stepsize);
          }
          break;
        }
      case 5 :
        {
          bool use_epsmin = 1;
          palenzuela(c2p_rep,S_squared,BdotS,B_squared,con,prim,g_up,g_lo,
            tol_x,use_epsmin);
          break;
        }
      case 6 :
        {
          NR_2D_Noble(c2p_rep,S_squared,BdotS,B_squared,con,prim,g_up,g_lo,
            tol_x);
          break;
        }
      case 7 :
        {
          newman(c2p_rep,S_squared,BdotS,B_squared,con,prim,g_up,g_lo,tol_x);
          break;
        }
      default :
        {
          printf("%s\n", "ERROR: No valid MHD con2prim scheme identifier!");
          exit(1);
          break;         
        }                  
    }
}


void con2prim_MHD_(double * prim, double const * con, const double g_up[4][4],
      const double g_lo[4][4], const bool key_excise, const bool c2p_grace,
      struct c2p_report * c2p_rep)
{
  
  bool reset2atmo = false;
  c2p_rep->failed = false;
  c2p_rep->retry  = false;
  c2p_rep->nEOScalls = 0;
  strcpy(c2p_rep->err_msg, "Everything all right!");
  
  double c2p_grace_fac = 1.0;
  double c2p_grace_atmo_fac = 1.0;
  if (c2p_grace) {
    c2p_grace_fac = 1e2;
    c2p_grace_atmo_fac = 4.0;
  }
  
  // check for NaNs in conservatives
  for(int l=0;l<c2p.numcons;l++){
    if (con[l] != con[l]){
      reset2atmo = true;
      c2p_rep->failed = true;
      strcpy( c2p_rep->err_msg, "NaN(s) in conservatives. Resetting to atmosphere.");
    }  
  }
  
  // calculate some quantities for c2p scheme
  double B_squared, S_squared, BdotS;
  calc_B2_S2_BdotS(con, g_up, g_lo, &B_squared, &S_squared, &BdotS);
  
  if ((B_squared != B_squared) || (S_squared != S_squared) || (BdotS != BdotS))
  {
    reset2atmo = true;
    c2p_rep->failed = true;
    strcpy( c2p_rep->err_msg, "NaN(s) in B2, S2, or BdotS. Resetting to atmosphere.");
  }
  
  
  if ((reset2atmo) || (key_excise) || (con[D] <= c2p.rho_atmo * (1.0+c2p.rho_atmo_tol*c2p_grace_atmo_fac))) 
  {
    // reset to atmosphere:
    // - if below density treshold 
    // - if in excised region
    // - if NaNs in conservatives
    reset_to_atmosphere(prim, con);
    c2p_rep->adjust_cons = true;
    c2p_rep->count = 0;
    
  } else {
          
    // perform recovery of primitives    
          
    // Adjust initial guess for 3-velocity to ensure v^2 <= 1.
    reset_velocity(g_up,prim);
    
    // keep original initial guesses to initialize retry (see below)
    double original_prim[c2p.numprims];
    for(int l=0;l<c2p.numprims;l++){
	    original_prim[l] = prim[l];
    }
    
    c2p_recovery_scheme(c2p.c2p_method,c2p_rep,S_squared,BdotS,B_squared,con,prim,original_prim,
          g_up,g_lo,c2p.tol_x*c2p_grace_fac);               
 
    if(c2p_rep->failed){

      // try backup scheme
      if (c2p.use_c2p_method_backup) {
        // reset primitives (initial guesses)
        for(int l=0;l<c2p.numprims;l++){
          prim[l] = original_prim[l];
        }
        c2p_recovery_scheme(c2p.c2p_method_backup,c2p_rep,S_squared,BdotS,B_squared,con,prim,original_prim,
          g_up,g_lo,c2p.tol_x*c2p_grace_fac); 
      }
      
      // retry with larger tolerance
// #if !STANDALONE
//       if(c2p_rep->failed){   
//         c2p_rep->retry = true;
      
//         // reset primitives (initial guesses)
//         for(int l=0;l<c2p.numprims;l++){
// 	        prim[l] = original_prim[l];
//         }

//         c2p_recovery_scheme(c2p.c2p_method,c2p_rep,S_squared,BdotS,B_squared,con,prim,original_prim,
//            g_up,g_lo,c2p.tol_x_retry*c2p_grace_fac);
//       }        
// #endif      
      if(c2p_rep->failed){
        // as a last resort, perform a prim2con and check whether we are actually 
        // reasonably consistent with the conservatives anyway and accept 
        // c2p attempt in this case
        //double con_check[c2p.numcons];
        //prim2con_MHD_(prim, con_check, g_up, g_lo);
        
        //c2p_rep->failed = false;
        //double deviation = 0.0;
        //for(int l=0;l<=7;l++){
        //  if (con[l] == 0.0){
        //    deviation = fabs(con_check[l]);
        //  }else{ 
        //    deviation = fabs((con_check[l] - con[l])/con[l]);
        //  }
        //  if ((deviation > 1e-5*c2p_grace_fac) || (deviation != deviation)){
        //    // con2prim really failed, reset to atmosphere
            reset2atmo = true;
            c2p_rep->failed = true;
            strcpy( c2p_rep->err_msg, "Con2Prim finally failed. Resetting to atmosphere.");
        //  }
        //}
      }
    }

    if ((reset2atmo) || (prim[RHO] <= c2p.rho_atmo * (1.0+c2p.rho_atmo_tol*c2p_grace_atmo_fac))) 
    {
      // reset to atmosphere
      reset_to_atmosphere(prim, con);
      c2p_rep->adjust_cons = true;
    }
    
    if ((c2p.evolve_T) && (prim[TEMP] < c2p.T_atmo)) {
      // reset temperature if below temperature threshold
      reset_temperature(prim);
      c2p_rep->adjust_cons = true;
    }

    if (c2p.enforce_v2) {
      // enforce that round off errors don't result in v2 >= 1!
      reset_velocity(g_up, prim);
    }
  }
}
